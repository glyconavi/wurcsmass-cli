# wurcsMass-cli

## build

```
git clone https://gitlab.com/glyconavi/wurcsmass-cli.git
```

```
cd wurcsmass-cli
```

```
gradle build
```

## Usage

* INPUT: WURCS STring

```
java -jar ./build/libs/wurcsmass-cli-all.jar "WURCS=2.0/2,2,1/[a2122m-1b_1-5_4*N][ad21h-1a_1-5_3*OC_4*NCC]/1-2/a2-b1"
```

* WURCS File

```
java -jar ./build/libs/wurcsmass-cli-all.jar ./testWURCS.txt 
```

  * Output of WURCS File

```
cat testWURCS.txt_2022-01-02.ttl 
```

```
<http://rdf.glycoinfo.org/glycan/c1be4cff4ef6ce1da14e360e09a3704f7b87e55c1c60a7aa0beb5b8251ccbbd7>
	<http://purl.jp/bio/12/glyco/glycan#has_monoisotopic_molecular_weight>
	"320.19473663"^^<http://www.w3.org/2001/XMLSchema#decimal> .
<http://rdf.glycoinfo.org/glycan/c1be4cff4ef6ce1da14e360e09a3704f7b87e55c1c60a7aa0beb5b8251ccbbd7>
	<http://purl.jp/bio/12/glyco/glycan#has_sequence>
	"WURCS=2.0/2,2,1/[a2122m-1b_1-5_4*N][ad21h-1a_1-5_3*OC_4*NCC]/1-2/a2-b1" .
<http://rdf.glycoinfo.org/glycan/c1be4cff4ef6ce1da14e360e09a3704f7b87e55c1c60a7aa0beb5b8251ccbbd7>
	<http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format>
	<http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> .
```


### rapper

```
rapper -i turtle -o ntriples out.ttl > out.ttl.nt
```


