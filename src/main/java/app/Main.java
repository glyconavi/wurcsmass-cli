package app;

import org.glycoinfo.WURCSFramework.util.array.WURCSImporter;
import org.glycoinfo.WURCSFramework.wurcs.array.WURCSArray;
import org.glycoinfo.WURCSFramework.util.array.mass.WURCSMassCalculator;
import org.glycoinfo.WURCSFramework.util.array.mass.WURCSMassException;
import org.glycoinfo.WURCSFramework.util.array.WURCSFormatException;

//import java.net.URL;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.Map;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.MessageDigest;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;

import java.math.BigDecimal;


public class Main {
  //static String version = "1.2.5";
  //static String acc = "G42666HT";
  //static String wurcs = "WURCS=2.0/1,2,1/[a2122h-1b_1-5_2*NCC/3=O]/1-1/a4-b1";

  public static void main(String[] args) throws Exception {
    String file = "";
    if(args.length == 1){
      file = args[0];
		
      if(file.contains("WURCS=2.0/")){
          System.out.println(getMassTurtle(file) + "\n");
      }
      else {
        LocalDateTime localDate = LocalDateTime.now();
        DateTimeFormatter date1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String stringDate1 = localDate.format(date1);

        String filepath = file + "." + stringDate1 + ".ttl";
        try (FileWriter writer = new FileWriter(filepath, false);){
          Path path = Paths.get(file);
          try (BufferedReader br = Files.newBufferedReader(path)) {
            String text;
            while ((text = br.readLine()) != null) {
              writer.write(getMassTurtle(text) + "\n");
            }
          } catch (IOException e) {
            //例外処理（パスが存在しないケースなど）
            System.out.print(e);
          }
        } catch (IOException e) {
          //例外処理（パスが存在しないケースなど）
          System.out.print(e);
        }
      }
    }
    else {
      System.out.println("java -jar app.jar {WURCSfile}\n");
    }
  }


  public static String getMassTurtle(String wurcs) throws Exception {
    String mass = "";
    String message = "";
    try{
      WURCSImporter t_objImporter = new WURCSImporter();
      WURCSArray t_objWURCS = t_objImporter.extractWURCSArray(wurcs);
      BigDecimal bd_mass = WURCSMassCalculator.calcMassWURCS(t_objWURCS);
      mass = bd_mass.toString();
    }catch(WURCSMassException e){
      e.printStackTrace();
      message = e.toString();
    }catch(WURCSFormatException e){
      e.printStackTrace();
      message = message + e.toString();
    }catch(Exception e){
      e.printStackTrace();
      message = message + e.toString();
    }

    StringBuilder sb = new StringBuilder();
    //String ttl = "";
/*
    ttl += "<http://identifiers.org/glytoucan/" + acc + ">\n";
    ttl += "\t<http://purl.jp/bio/12/glyco/glycan#has_monoisotopic_molecular_weight>\n";
    ttl += "\t\"" + mass + "\"^^<http://www.w3.org/2001/XMLSchema#decimal> .\n";

    ttl += "<http://identifiers.org/glytoucan/" + acc + ">\n";
    ttl += "\t<http://purl.jp/bio/12/glyco/glycan#has_glycosequence>\n";
    ttl += "\t<http://rdf.glycoinfo.org/glycan/" + retSha256(wurcs) + "> .\n";
*/
    if (mass != "") {
      sb.append("<http://rdf.glycoinfo.org/glycan/" + retSha256(wurcs) + ">\n");
      sb.append("\t<http://purl.jp/bio/12/glyco/glycan#has_monoisotopic_molecular_weight>\n");
      sb.append("\t\"" + mass + "\"^^<http://www.w3.org/2001/XMLSchema#decimal> .\n");
    }

    sb.append("<http://rdf.glycoinfo.org/glycan/" + retSha256(wurcs) + ">\n");
    sb.append("\t<http://purl.jp/bio/12/glyco/glycan#has_sequence>\n");
    sb.append("\t\"" + wurcs + "\" .\n");

    sb.append("<http://rdf.glycoinfo.org/glycan/" + retSha256(wurcs) + ">\n");
    sb.append("\t<http://purl.jp/bio/12/glyco/glycan#in_carbohydrate_format>\n");
    sb.append("\t<http://purl.jp/bio/12/glyco/glycan#carbohydrate_format_wurcs> .\n");

    return sb.toString();
  }

  	public static String retSha256(String str) {
		String text = str;
		byte[] cipher_byte;
		try{
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(text.getBytes());
			cipher_byte = md.digest();
			StringBuilder sb = new StringBuilder(2 * cipher_byte.length);
			for(byte b: cipher_byte) {
				sb.append(String.format("%02x", b&0xff) );
			}
			text = sb.toString();
		} catch (Exception e) { 
			//e.printStackTrace();
		}
		return text;
	}




}

